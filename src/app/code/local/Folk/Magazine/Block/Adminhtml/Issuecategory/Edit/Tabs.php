<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category admin edit tabs
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Adminhtml_Issuecategory_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('issuecategory_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('folk_magazine')->__('Issue Category'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Folk_Magazine_Block_Adminhtml_Issuecategory_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_issuecategory',
            array(
                'label'   => Mage::helper('folk_magazine')->__('Issue Category'),
                'title'   => Mage::helper('folk_magazine')->__('Issue Category'),
                'content' => $this->getLayout()->createBlock(
                    'folk_magazine/adminhtml_issuecategory_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_meta_issuecategory',
            array(
                'label'   => Mage::helper('folk_magazine')->__('Meta'),
                'title'   => Mage::helper('folk_magazine')->__('Meta'),
                'content' => $this->getLayout()->createBlock(
                    'folk_magazine/adminhtml_issuecategory_edit_tab_meta'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_issuecategory',
                array(
                    'label'   => Mage::helper('folk_magazine')->__('Store views'),
                    'title'   => Mage::helper('folk_magazine')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'folk_magazine/adminhtml_issuecategory_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        $this->addTab(
            'categories',
            array(
                'label' => Mage::helper('folk_magazine')->__('Associated categories'),
                'url'   => $this->getUrl('*/*/categories', array('_current' => true)),
                'class' => 'ajax'
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve issue category entity
     *
     * @access public
     * @return Folk_Magazine_Model_Issuecategory
     * @author Ultimate Module Creator
     */
    public function getIssuecategory()
    {
        return Mage::registry('current_issuecategory');
    }
}
