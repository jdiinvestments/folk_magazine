<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Category helper
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Helper_Category extends Folk_Magazine_Helper_Data
{

    /**
     * get the selected issues for a category
     *
     * @access public
     * @param Mage_Catalog_Model_Category $category
     * @return array()
     * @author Ultimate Module Creator
     */
    public function getSelectedIssues(Mage_Catalog_Model_Category $category)
    {
        if (!$category->hasSelectedIssues()) {
            $issues = array();
            foreach ($this->getSelectedIssuesCollection($category) as $issue) {
                $issues[] = $issue;
            }
            $category->setSelectedIssues($issues);
        }
        return $category->getData('selected_issues');
    }

    /**
     * get issue collection for a category
     *
     * @access public
     * @param Mage_Catalog_Model_Category $category
     * @return Folk_Magazine_Model_Resource_Issue_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedIssuesCollection(Mage_Catalog_Model_Category $category)
    {
        $collection = Mage::getResourceSingleton('folk_magazine/issue_collection')
            ->addCategoryFilter($category);
        return $collection;
    }

    /**
     * get the selected issue categories for a category
     *
     * @access public
     * @param Mage_Catalog_Model_Category $category
     * @return array()
     * @author Ultimate Module Creator
     */
    public function getSelectedIssuecategories(Mage_Catalog_Model_Category $category)
    {
        if (!$category->hasSelectedIssuecategories()) {
            $issuecategories = array();
            foreach ($this->getSelectedIssuecategoriesCollection($category) as $issuecategory) {
                $issuecategories[] = $issuecategory;
            }
            $category->setSelectedIssuecategories($issuecategories);
        }
        return $category->getData('selected_issuecategories');
    }

    /**
     * get issue category collection for a category
     *
     * @access public
     * @param Mage_Catalog_Model_Category $category
     * @return Folk_Magazine_Model_Resource_Issuecategory_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedIssuecategoriesCollection(Mage_Catalog_Model_Category $category)
    {
        $collection = Mage::getResourceSingleton('folk_magazine/issuecategory_collection')
            ->addCategoryFilter($category);
        return $collection;
    }
}
