<?php

class Folk_Magazine_Block_Adminhtml_Issue_Edit_Tab_Feature extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('issue_feature_grid');
        $this->setDefaultSort('sort_order');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        //$this->setTemplate('folk/content/widget/grid.phtml');
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->setChild('add_feature_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => $this->__('Add Feature'),
                    'id' => 'add_feature_button',
                    'onclick' => 'window.location.href=\'' . $this->getUrl('*/feature/new/') . '\'',
                    'class' => 'task',
                    'title' => 'Add Feature'
                ))
        );
        return parent::_prepareLayout();
    }


    public function getMainButtonsHtml()
    {
        $html = '';
        //$html .= parent::getMainButtonsHtml();
        if ($this->getFilterVisibility()) {
            $html .= $this->getChildHtml('add_feature_button');
        }
        return $html;
    }

    protected function _prepareCollection()
    {
        $collection = Mage::helper('folk_content')->getFeatureCollection(Mage::registry('current_issue')->getFeatures());

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('feature_id', array(
            'header' => $this->__('ID'),
            'sortable' => true,
            'width' => '60px',
            'index' => 'feature_id'
        ));

        $this->addColumn('sort_order', array(
            'header' => $this->__('Order'),
            'sortable' => true,
            'width' => '30px',
            'index' => 'sort_order'
        ));

        $this->addColumn('title', array(
            'header' => $this->__('Title'),
            'sortable' => true,
            'index' => 'title',
            'column_css_class' => 'title'
        ));

        $this->addColumn('enabled', array(
            'header' => $this->__('Enabled'),
            'index' => 'enabled',
            'type' => 'options',
            'options' => array(
                0 => 'Disabled',
                1 => 'Enabled'
            )
        ));

        $this->addColumn('action', array(
            'header' => $this->__('Action'),
            'width' => '100px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => $this->__('Edit'),
                    'url' => array('base' => '*/feature/edit/'),
                    'field' => 'id',
                ),
                array(
                    'caption' => $this->__('Delete'),
                    'url' => array('base' => '*/feature/delete/'),
                    'field' => 'id',
                ),

            ),
            'filter' => false,
            'sortable' => false,
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getData('grid_url') ? $this->getData('grid_url') : $this->getUrl('*/*/*', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/feature/edit/', array('id' => $row->getId()));
    }
}
