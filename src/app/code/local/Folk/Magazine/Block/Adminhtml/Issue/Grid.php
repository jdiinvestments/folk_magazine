<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue admin grid block
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Adminhtml_Issue_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('issueGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Folk_Magazine_Block_Adminhtml_Issue_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('folk_magazine/issue')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Folk_Magazine_Block_Adminhtml_Issue_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('folk_magazine')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'issuecategory_id',
            array(
                'header'    => Mage::helper('folk_magazine')->__('Issue Category'),
                'index'     => 'issuecategory_id',
                'type'      => 'options',
                'options'   => Mage::getResourceModel('folk_magazine/issuecategory_collection')
                    ->toOptionHash(),
                'renderer'  => 'folk_magazine/adminhtml_helper_column_renderer_parent',
                'params'    => array(
                    'id'    => 'getIssuecategoryId'
                ),
                'base_link' => 'adminhtml/magazine_issuecategory/edit'
            )
        );
        $this->addColumn(
            'title',
            array(
                'header'    => Mage::helper('folk_magazine')->__('Title'),
                'align'     => 'left',
                'index'     => 'title',
            )
        );
        
        $this->addColumn(
            'status',
            array(
                'header'  => Mage::helper('folk_magazine')->__('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => array(
                    '1' => Mage::helper('folk_magazine')->__('Enabled'),
                    '0' => Mage::helper('folk_magazine')->__('Disabled'),
                )
            )
        );
        $this->addColumn(
            'make_active_from',
            array(
                'header' => Mage::helper('folk_magazine')->__('Make active from'),
                'index'  => 'make_active_from',
                'type'=> 'date',

            )
        );
        $this->addColumn(
            'make_active_to',
            array(
                'header' => Mage::helper('folk_magazine')->__('Make active to'),
                'index'  => 'make_active_to',
                'type'=> 'date',

            )
        );
        $this->addColumn(
            'features',
            array(
                'header' => Mage::helper('folk_magazine')->__('Features'),
                'index'  => 'features',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'url_key',
            array(
                'header' => Mage::helper('folk_magazine')->__('URL key'),
                'index'  => 'url_key',
            )
        );
        if (!Mage::app()->isSingleStoreMode() && !$this->_isExport) {
            $this->addColumn(
                'store_id',
                array(
                    'header'     => Mage::helper('folk_magazine')->__('Store Views'),
                    'index'      => 'store_id',
                    'type'       => 'store',
                    'store_all'  => true,
                    'store_view' => true,
                    'sortable'   => false,
                    'filter_condition_callback'=> array($this, '_filterStoreCondition'),
                )
            );
        }
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('folk_magazine')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'updated_at',
            array(
                'header'    => Mage::helper('folk_magazine')->__('Updated at'),
                'index'     => 'updated_at',
                'width'     => '120px',
                'type'      => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('folk_magazine')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('folk_magazine')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('folk_magazine')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('folk_magazine')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('folk_magazine')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Folk_Magazine_Block_Adminhtml_Issue_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('issue');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('folk_magazine')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('folk_magazine')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('folk_magazine')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('folk_magazine')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('folk_magazine')->__('Enabled'),
                            '0' => Mage::helper('folk_magazine')->__('Disabled'),
                        )
                    )
                )
            )
        );
        $values = Mage::getResourceModel('folk_magazine/issuecategory_collection')->toOptionHash();
        $values = array_reverse($values, true);
        $values[''] = '';
        $values = array_reverse($values, true);
        $this->getMassactionBlock()->addItem(
            'issuecategory_id',
            array(
                'label'      => Mage::helper('folk_magazine')->__('Change Issue Category'),
                'url'        => $this->getUrl('*/*/massIssuecategoryId', array('_current'=>true)),
                'additional' => array(
                    'flag_issuecategory_id' => array(
                        'name'   => 'flag_issuecategory_id',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('folk_magazine')->__('Issue Category'),
                        'values' => $values
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Folk_Magazine_Model_Issue
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Folk_Magazine_Block_Adminhtml_Issue_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * filter store column
     *
     * @access protected
     * @param Folk_Magazine_Model_Resource_Issue_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return Folk_Magazine_Block_Adminhtml_Issue_Grid
     * @author Ultimate Module Creator
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->addStoreFilter($value);
        return $this;
    }
}
