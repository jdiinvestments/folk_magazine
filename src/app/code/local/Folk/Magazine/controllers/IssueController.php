<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue front contrller
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_IssueController extends Mage_Core_Controller_Front_Action
{

    /**
      * default action
      *
      * @access public
      * @return void
      * @author Ultimate Module Creator
      */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if (Mage::helper('folk_magazine/issue')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label' => Mage::helper('folk_magazine')->__('Home'),
                        'link'  => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'issues',
                    array(
                        'label' => Mage::helper('folk_magazine')->__('Issues'),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', Mage::helper('folk_magazine/issue')->getIssuesUrl());
        }
        if ($headBlock) {
            $headBlock->setTitle(Mage::getStoreConfig('folk_magazine/issue/meta_title'));
            $headBlock->setKeywords(Mage::getStoreConfig('folk_magazine/issue/meta_keywords'));
            $headBlock->setDescription(Mage::getStoreConfig('folk_magazine/issue/meta_description'));
        }
        $this->renderLayout();
    }

    /**
     * init Issue
     *
     * @access protected
     * @return Folk_Magazine_Model_Issue
     * @author Ultimate Module Creator
     */
    protected function _initIssue()
    {
        $issueId   = $this->getRequest()->getParam('id', 0);
        $issue     = Mage::getModel('folk_magazine/issue')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($issueId);
        if (!$issue->getId()) {
            return false;
        } elseif (!$issue->getStatus()) {
            return false;
        }
        return $issue;
    }

    /**
     * view issue action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function viewAction()
    {
        $issue = $this->_initIssue();
        if (!$issue) {
            $this->_forward('no-route');
            return;
        }
        Mage::register('current_issue', $issue);
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('magazine-issue magazine-issue' . $issue->getId());
        }
        if (Mage::helper('folk_magazine/issue')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label'    => Mage::helper('folk_magazine')->__('Home'),
                        'link'     => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'issues',
                    array(
                        'label' => Mage::helper('folk_magazine')->__('Issues'),
                        'link'  => Mage::helper('folk_magazine/issue')->getIssuesUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'issue',
                    array(
                        'label' => $issue->getTitle(),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $issue->getIssueUrl());
        }
        if ($headBlock) {
            if ($issue->getMetaTitle()) {
                $headBlock->setTitle($issue->getMetaTitle());
            } else {
                $headBlock->setTitle($issue->getTitle());
            }
            $headBlock->setKeywords($issue->getMetaKeywords());
            $headBlock->setDescription($issue->getMetaDescription());
        }
        $this->renderLayout();
    }
}
