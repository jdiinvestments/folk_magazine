-- add table prefix if you have one
DELETE FROM eav_attribute WHERE attribute_code = 'folk_magazine_issue' AND entity_type_id IN (SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code = 'catalog_category');
DROP TABLE IF EXISTS folk_magazine_issue_category;
DROP TABLE IF EXISTS folk_magazine_issue_store;
DROP TABLE IF EXISTS folk_magazine_issue;
DROP TABLE IF EXISTS folk_magazine_issuecategory_category;
DROP TABLE IF EXISTS folk_magazine_issuecategory_store;
DROP TABLE IF EXISTS folk_magazine_issuecategory;
DELETE FROM core_resource WHERE code = 'folk_magazine_setup';
DELETE FROM core_config_data WHERE path like 'folk_magazine/%';