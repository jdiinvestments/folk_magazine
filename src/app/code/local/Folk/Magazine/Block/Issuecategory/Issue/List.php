<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category Issues list block
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Issuecategory_Issue_List extends Folk_Magazine_Block_Issue_List
{
    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $issuecategory = $this->getIssuecategory();
        if ($issuecategory) {
            $today = date('Y-m-d 00:00:00');
            $this->getIssues()
                ->addFieldToFilter('status', 1)
                ->addfieldtofilter('make_active_from',
                    array(
                        array('to' => $today),
                        array('make_active_from', 'null' => '')))
                ->addfieldtofilter('make_active_to',
                    array(
                        array('gteq' => $today),
                        array('make_active_to', 'null' => ''))
                )
                ->addFieldToFilter('issuecategory_id', $issuecategory->getId());
        }
    }

    /**
     * prepare the layout - actually do nothing
     *
     * @access protected
     * @return Folk_Magazine_Block_Issuecategory_Issue_List
     * @author Ultimate Module Creator
     */
    protected function _prepareLayout()
    {
        return $this;
    }

    /**
     * get the current issue category
     *
     * @access public
     * @return Folk_Magazine_Model_Issuecategory
     * @author Ultimate Module Creator
     */
    public function getIssuecategory()
    {
        return Mage::registry('current_issuecategory');
    }
}
