<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category view block
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Issuecategory_View extends Mage_Core_Block_Template
{
    /**
     * get the current issue category
     *
     * @access public
     * @return mixed (Folk_Magazine_Model_Issuecategory|null)
     * @author Ultimate Module Creator
     */
    public function getCurrentIssuecategory()
    {
        return Mage::registry('current_issuecategory');
    }

    public function getIssues()
    {
        $today = date('Y-m-d 00:00:00');
        $issues = $this->getCurrentIssuecategory()->getSelectedIssuesCollection()
            ->addFieldToFilter('status', 1)
            ->addfieldtofilter('make_active_from',
                array(
                    array('to' => $today),
                    array('make_active_from', 'null' => '')))
            ->addfieldtofilter('make_active_to',
                array(
                    array('gteq' => $today),
                    array('make_active_to', 'null' => ''))
            )
            ->addFieldToFilter('issuecategory_id', $this->getCurrentIssuecategory()->getId());
        return $issues;
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return Folk_Magazine_Block_Category_List
     *
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $issues = $this->getIssues();
        $pager = $this->getLayout()->createBlock(
            'page/html_pager',
            'folk_magazine.issuecategory.html.pager'
        )
            ->setCollection($issues);
        $this->setChild('pager', $pager);
        $issues->load();
        return $this;
    }

    /**
     * get the pager html
     *
     * @access public
     * @return string
     *
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
