<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue edit form tab
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Adminhtml_Issue_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Folk_Magazine_Block_Adminhtml_Issue_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('issue_');
        $form->setFieldNameSuffix('issue');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'issue_form',
            array('legend' => Mage::helper('folk_magazine')->__('Issue'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('folk_magazine/adminhtml_issue_helper_image')
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $values = Mage::getResourceModel('folk_magazine/issuecategory_collection')
            ->toOptionArray();
        array_unshift($values, array('label' => '', 'value' => ''));

        $html = '<a href="{#url}" id="issue_issuecategory_id_link" target="_blank"></a>';
        $html .= '<script type="text/javascript">
            function changeIssuecategoryIdLink() {
                if ($(\'issue_issuecategory_id\').value == \'\') {
                    $(\'issue_issuecategory_id_link\').hide();
                } else {
                    $(\'issue_issuecategory_id_link\').show();
                    var url = \''.$this->getUrl('adminhtml/magazine_issuecategory/edit', array('id'=>'{#id}', 'clear'=>1)).'\';
                    var text = \''.Mage::helper('core')->escapeHtml($this->__('View {#name}')).'\';
                    var realUrl = url.replace(\'{#id}\', $(\'issue_issuecategory_id\').value);
                    $(\'issue_issuecategory_id_link\').href = realUrl;
                    $(\'issue_issuecategory_id_link\').innerHTML = text.replace(\'{#name}\', $(\'issue_issuecategory_id\').options[$(\'issue_issuecategory_id\').selectedIndex].innerHTML);
                }
            }
            $(\'issue_issuecategory_id\').observe(\'change\', changeIssuecategoryIdLink);
            changeIssuecategoryIdLink();
            </script>';

        $fieldset->addField(
            'issuecategory_id',
            'select',
            array(
                'label'     => Mage::helper('folk_magazine')->__('Issue Category'),
                'name'      => 'issuecategory_id',
                'required'  => false,
                'values'    => $values,
                'after_element_html' => $html
            )
        );

        $fieldset->addField(
            'title',
            'text',
            array(
                'label' => Mage::helper('folk_magazine')->__('Title'),
                'name'  => 'title',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'subtitle',
            'textarea',
            array(
                'label' => Mage::helper('folk_magazine')->__('Subtitle'),
                'name'  => 'subtitle',

           )
        );

        $fieldset->addField(
            'image',
            'image',
            array(
                'label' => Mage::helper('folk_magazine')->__('Image'),
                'name'  => 'image',

           )
        );

        $fieldset->addField(
            'make_active_from',
            'date',
            array(
                'label' => Mage::helper('folk_magazine')->__('Make active from'),
                'name'  => 'make_active_from',

            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format'  => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
           )
        );

        $fieldset->addField(
            'make_active_to',
            'date',
            array(
                'label' => Mage::helper('folk_magazine')->__('Make active to'),
                'name'  => 'make_active_to',

            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format'  => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
           )
        );

        $fieldset->addField(
            'html_content',
            'editor',
            array(
                'label' => Mage::helper('folk_magazine')->__('Html Content'),
                'name'  => 'html_content',
            'config' => $wysiwygConfig,

           )
        );

        $fieldset->addField(
            'features',
            'text',
            array(
                'label' => Mage::helper('folk_magazine')->__('Features'),
                'name'  => 'features',

           )
        );
        $fieldset->addField(
            'url_key',
            'text',
            array(
                'label' => Mage::helper('folk_magazine')->__('Url key'),
                'name'  => 'url_key',
                'note'  => Mage::helper('folk_magazine')->__('Relative to Website Base URL')
            )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('folk_magazine')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('folk_magazine')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('folk_magazine')->__('Disabled'),
                    ),
                ),
            )
        );
        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_issue')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_issue')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getIssueData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getIssueData());
            Mage::getSingleton('adminhtml/session')->setIssueData(null);
        } elseif (Mage::registry('current_issue')) {
            $formValues = array_merge($formValues, Mage::registry('current_issue')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
