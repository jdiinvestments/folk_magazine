<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category list on category page block
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Catalog_Category_List_Issuecategory extends Mage_Core_Block_Template
{
    /**
     * get the list of issue categories
     *
     * @access protected
     * @return Folk_Magazine_Model_Resource_Issuecategory_Collection
     * @author Ultimate Module Creator
     */
    public function getIssuecategoryCollection()
    {
        if (!$this->hasData('issuecategory_collection')) {
            $category = Mage::registry('current_category');
            $collection = Mage::getResourceSingleton('folk_magazine/issuecategory_collection')
                ->addStoreFilter(Mage::app()->getStore())
                ->addFieldToFilter('status', 1)
                ->addCategoryFilter($category);
            $collection->getSelect()->order('related_category.position', 'ASC');
            $this->setData('issuecategory_collection', $collection);
        }
        return $this->getData('issuecategory_collection');
    }
}
