<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category category model
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Model_Issuecategory_Category extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->_init('folk_magazine/issuecategory_category');
    }

    /**
     * Save data for issue category-category relation
     *
     * @access public
     * @param  Folk_Magazine_Model_Issuecategory $issuecategory
     * @return Folk_Magazine_Model_Issuecategory_Category
     * @author Ultimate Module Creator
     */
    public function saveIssuecategoryRelation($issuecategory)
    {
        $data = $issuecategory->getCategoriesData();
        if (!is_null($data)) {
            $this->_getResource()->saveIssuecategoryRelation($issuecategory, $data);
        }
        return $this;
    }

    /**
     * get categories for issue category
     *
     * @access public
     * @param Folk_Magazine_Model_Issuecategory $issuecategory
     * @return Folk_Magazine_Model_Resource_Issuecategory_Category_Collection
     * @author Ultimate Module Creator
     */
    public function getCategoryCollection($issuecategory)
    {
        $collection = Mage::getResourceModel('folk_magazine/issuecategory_category_collection')
            ->addIssuecategoryFilter($issuecategory);
        return $collection;
    }
}
