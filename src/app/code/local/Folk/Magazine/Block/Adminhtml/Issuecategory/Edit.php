<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category admin edit form
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Adminhtml_Issuecategory_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'folk_magazine';
        $this->_controller = 'adminhtml_issuecategory';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('folk_magazine')->__('Save Issue Category')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('folk_magazine')->__('Delete Issue Category')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('folk_magazine')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_issuecategory') && Mage::registry('current_issuecategory')->getId()) {
            return Mage::helper('folk_magazine')->__(
                "Edit Issue Category '%s'",
                $this->escapeHtml(Mage::registry('current_issuecategory')->getTitle())
            );
        } else {
            return Mage::helper('folk_magazine')->__('Add Issue Category');
        }
    }
}
