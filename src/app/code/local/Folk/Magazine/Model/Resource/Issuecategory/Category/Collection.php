<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category - Category relation resource model collection
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Model_Resource_Issuecategory_Category_Collection extends Mage_Catalog_Model_Resource_Category_Collection
{
    /**
     * remember if fields have been joined
     *
     * @var bool
     */
    protected $_joinedFields = false;

    /**
     * join the link table
     *
     * @access public
     * @return Folk_Magazine_Model_Resource_Issuecategory_Category_Collection
     * @author Ultimate Module Creator
     */
    public function joinFields()
    {
        if (!$this->_joinedFields) {
            $this->getSelect()->join(
                array('related' => $this->getTable('folk_magazine/issuecategory_category')),
                'related.category_id = e.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    /**
     * add issue category filter
     *
     * @access public
     * @param Folk_Magazine_Model_Issuecategory | int $issuecategory
     * @return Folk_Magazine_Model_Resource_Issuecategory_Category_Collection
     * @author Ultimate Module Creator
     */
    public function addIssuecategoryFilter($issuecategory)
    {
        if ($issuecategory instanceof Folk_Magazine_Model_Issuecategory) {
            $issuecategory = $issuecategory->getId();
        }
        if (!$this->_joinedFields) {
            $this->joinFields();
        }
        $this->getSelect()->where('related.issuecategory_id = ?', $issuecategory);
        return $this;
    }
}
