<?php 
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue helper
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Helper_Issue extends Mage_Core_Helper_Abstract
{

    /**
     * get the url to the issues list page
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getIssuesUrl()
    {
        if ($listKey = Mage::getStoreConfig('folk_magazine/issue/url_rewrite_list')) {
            return Mage::getUrl('', array('_direct'=>$listKey));
        }
        return Mage::getUrl('folk_magazine/issue/index');
    }

    /**
     * check if breadcrumbs can be used
     *
     * @access public
     * @return bool
     * @author Ultimate Module Creator
     */
    public function getUseBreadcrumbs()
    {
        return Mage::getStoreConfigFlag('folk_magazine/issue/breadcrumbs');
    }
}
