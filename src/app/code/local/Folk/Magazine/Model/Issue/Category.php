<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue category model
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Model_Issue_Category extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource
     *
     * @access protected
     * @return void
     * @author Ultimate Module Creator
     */
    protected function _construct()
    {
        $this->_init('folk_magazine/issue_category');
    }

    /**
     * Save data for issue-category relation
     *
     * @access public
     * @param  Folk_Magazine_Model_Issue $issue
     * @return Folk_Magazine_Model_Issue_Category
     * @author Ultimate Module Creator
     */
    public function saveIssueRelation($issue)
    {
        $data = $issue->getCategoriesData();
        if (!is_null($data)) {
            $this->_getResource()->saveIssueRelation($issue, $data);
        }
        return $this;
    }

    /**
     * get categories for issue
     *
     * @access public
     * @param Folk_Magazine_Model_Issue $issue
     * @return Folk_Magazine_Model_Resource_Issue_Category_Collection
     * @author Ultimate Module Creator
     */
    public function getCategoryCollection($issue)
    {
        $collection = Mage::getResourceModel('folk_magazine/issue_category_collection')
            ->addIssueFilter($issue);
        return $collection;
    }
}
