<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category tab on category edit form
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Adminhtml_Catalog_Category_Tab_Issuecategory extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('catalog_category_issuecategory');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        if ($this->getCategory()->getId()) {
            $this->setDefaultFilter(array('in_issuecategories'=>1));
        }
    }

    /**
     * get current category
     *
     * @access public
     * @return Mage_Catalog_Model_Category|null
     * @author Ultimate Module Creator
     */
    public function getCategory()
    {
        return Mage::registry('current_category');
    }

    /**
     * prepare the collection
     *
     * @access protected
     * @return Folk_Magazine_Block_Adminhtml_Catalog_Category_Tab_Issuecategory
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('folk_magazine/issuecategory_collection');
        if ($this->getCategory()->getId()) {
            $constraint = 'related.category_id='.$this->getCategory()->getId();
        } else {
            $constraint = 'related.category_id=0';
        }
        $collection->getSelect()->joinLeft(
            array('related' => $collection->getTable('folk_magazine/issuecategory_category')),
            'related.issuecategory_id=main_table.entity_id AND '.$constraint,
            array('position')
        );
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * Prepare the columns
     *
     * @access protected
     * @return Folk_Magazine_Block_Adminhtml_Catalog_Category_Tab_Issuecategory
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_issuecategories',
            array(
                'header_css_class'  => 'a-center',
                'type'   => 'checkbox',
                'name'   => 'in_issuecategories',
                'values' => $this->_getSelectedIssuecategories(),
                'align'  => 'center',
                'index'  => 'entity_id'
            )
        );
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('folk_magazine')->__('Id'),
                'type'   => 'number',
                'align'  => 'left',
                'index'  => 'entity_id',
            )
        );
        $this->addColumn(
            'title',
            array(
                'header' => Mage::helper('folk_magazine')->__('Title'),
                'align'  => 'left',
                'index'  => 'title',
                'renderer' => 'folk_magazine/adminhtml_helper_column_renderer_relation',
                'params' => array(
                    'id' => 'getId'
                ),
                'base_link' => 'adminhtml/magazine_issuecategory/edit',
            )
        );
        $this->addColumn(
            'position',
            array(
                'header'         => Mage::helper('folk_magazine')->__('Position'),
                'name'           => 'position',
                'width'          => 60,
                'type'           => 'number',
                'validate_class' => 'validate-number',
                'index'          => 'position',
                'editable'       => true,
            )
        );
        return parent::_prepareColumns();
    }

    /**
     * Retrieve selected issuecategories
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    protected function _getSelectedIssuecategories()
    {
        $issuecategories = $this->getCategoryIssuecategories();
        if (!is_array($issuecategories)) {
            $issuecategories = array_keys($this->getSelectedIssuecategories());
        }
        return $issuecategories;
    }

    /**
     * Retrieve selected issuecategories
     *
     * @access protected
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedIssuecategories()
    {
        $issuecategories = array();
        //used helper here in order not to override the category model
        $selected = Mage::helper('folk_magazine/category')->getSelectedIssuecategories(Mage::registry('current_category'));
        if (!is_array($selected)) {
            $selected = array();
        }
        foreach ($selected as $issuecategory) {
            $issuecategories[$issuecategory->getId()] = array('position' => $issuecategory->getPosition());
        }
        return $issuecategories;
    }

    /**
     * get row url
     *
     * @access public
     * @param Folk_Magazine_Model_Issuecategory
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($item)
    {
        return '#';
    }

    /**
     * get grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            'adminhtml/magazine_issuecategory_catalog_category/issuecategoriesgrid',
            array(
                'id'=>$this->getCategory()->getId()
            )
        );
    }

    /**
     * Add filter
     *
     * @access protected
     * @param object $column
     * @return Folk_Magazine_Block_Adminhtml_Catalog_Category_Tab_Issuecategory
     * @author Ultimate Module Creator
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_issuecategories') {
            $issuecategoryIds = $this->_getSelectedIssuecategories();
            if (empty($issuecategoryIds)) {
                $issuecategoryIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$issuecategoryIds));
            } else {
                if ($issuecategoryIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$issuecategoryIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
}
