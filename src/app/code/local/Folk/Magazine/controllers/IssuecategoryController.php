<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category front contrller
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_IssuecategoryController extends Mage_Core_Controller_Front_Action
{

    /**
      * default action
      *
      * @access public
      * @return void
      * @author Ultimate Module Creator
      */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if (Mage::helper('folk_magazine/issuecategory')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label' => Mage::helper('folk_magazine')->__('Home'),
                        'link'  => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'issuecategories',
                    array(
                        'label' => Mage::helper('folk_magazine')->__('Issue Categories'),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', Mage::helper('folk_magazine/issuecategory')->getIssuecategoriesUrl());
        }
        if ($headBlock) {
            $headBlock->setTitle(Mage::getStoreConfig('folk_magazine/issuecategory/meta_title'));
            $headBlock->setKeywords(Mage::getStoreConfig('folk_magazine/issuecategory/meta_keywords'));
            $headBlock->setDescription(Mage::getStoreConfig('folk_magazine/issuecategory/meta_description'));
        }
        $this->renderLayout();
    }

    /**
     * init Issue Category
     *
     * @access protected
     * @return Folk_Magazine_Model_Issuecategory
     * @author Ultimate Module Creator
     */
    protected function _initIssuecategory()
    {
        $issuecategoryId   = $this->getRequest()->getParam('id', 0);
        $issuecategory     = Mage::getModel('folk_magazine/issuecategory')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($issuecategoryId);
        if (!$issuecategory->getId()) {
            return false;
        } elseif (!$issuecategory->getStatus()) {
            return false;
        }
        return $issuecategory;
    }

    /**
     * view issue category action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function viewAction()
    {
        $issuecategory = $this->_initIssuecategory();
        if (!$issuecategory) {
            $this->_forward('no-route');
            return;
        }
        Mage::register('current_issuecategory', $issuecategory);
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('magazine-issuecategory magazine-issuecategory' . $issuecategory->getId());
        }
        if (Mage::helper('folk_magazine/issuecategory')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label'    => Mage::helper('folk_magazine')->__('Home'),
                        'link'     => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'issuecategories',
                    array(
                        'label' => Mage::helper('folk_magazine')->__('Issue Categories'),
                        'link'  => Mage::helper('folk_magazine/issuecategory')->getIssuecategoriesUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'issuecategory',
                    array(
                        'label' => $issuecategory->getTitle(),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $issuecategory->getIssuecategoryUrl());
        }
        if ($headBlock) {
            if ($issuecategory->getMetaTitle()) {
                $headBlock->setTitle($issuecategory->getMetaTitle());
            } else {
                $headBlock->setTitle($issuecategory->getTitle());
            }
            $headBlock->setKeywords($issuecategory->getMetaKeywords());
            $headBlock->setDescription($issuecategory->getMetaDescription());
        }
        $this->renderLayout();
    }
}
