<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue admin controller
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Adminhtml_Magazine_IssueController extends Folk_Magazine_Controller_Adminhtml_Magazine
{
    /**
     * init the issue
     *
     * @access protected
     * @return Folk_Magazine_Model_Issue
     */
    protected function _initIssue()
    {
        $issueId  = (int) $this->getRequest()->getParam('id');
        $issue    = Mage::getModel('folk_magazine/issue');
        if ($issueId) {
            $issue->load($issueId);
        }
        Mage::register('current_issue', $issue);
        return $issue;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('folk_magazine')->__('Folk Magazine'))
             ->_title(Mage::helper('folk_magazine')->__('Issues'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit issue - action
     *
     * @access public
     * @return void
     *
     */
    public function editAction()
    {
        $issueId = $this->getRequest()->getParam('id');
        $issue = $this->_initIssue();

        if ($issueId) {
            if (!$issue->load($issueId)->getId()) {
                Mage::throwException($this->__('No record with ID "%s" found.', $issueId));
            }
            $returnUrl = '*/magazine_issue/updateissue/id/' . $issue->getId() . '/';
            Mage::helper('folk_content')->setReturnUrl($returnUrl);
        }

        if ($issueId && !$issue->getId()) {
            $this->_getSession()->addError(
                Mage::helper('folk_magazine')->__('This issue no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getIssueData(true);
        if (!empty($data)) {
            $issue->setData($data);
        }
        Mage::register('issue_data', $issue);
        $this->loadLayout();
        $this->_title(Mage::helper('folk_magazine')->__('Folk Magazine'))
             ->_title(Mage::helper('folk_magazine')->__('Issues'));
        if ($issue->getId()) {
            $this->_title($issue->getTitle());
        } else {
            $this->_title(Mage::helper('folk_magazine')->__('Add issue'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new issue action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save issue - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('issue')) {
            try {
                $data = $this->_filterDates($data, array('make_active_from' ,'make_active_to'));
                $issue = $this->_initIssue();
                $issue->addData($data);
                $imageName = $this->_uploadAndGetName(
                    'image',
                    Mage::helper('folk_magazine/issue_image')->getImageBaseDir(),
                    $data
                );
                $issue->setData('image', $imageName);
                $categories = $this->getRequest()->getPost('category_ids', -1);
                if ($categories != -1) {
                    $categories = explode(',', $categories);
                    $categories = array_unique($categories);
                    $issue->setCategoriesData($categories);
                }
                $issue->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('folk_magazine')->__('Issue was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $issue->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setIssueData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was a problem saving the issue.')
                );
                Mage::getSingleton('adminhtml/session')->setIssueData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('folk_magazine')->__('Unable to find issue to save.')
        );
        $this->_redirect('*/*/');
    }

    /*
     * This controller/action is very important. The Return url points at this entry point and therefore this
     * action needs to deal with four different cases [back] [create] [update] and [delete].
     */
    public function updateissueAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('folk_magazine/issue');

        // We can only process the return data if there's an issue id to attach it to
        if ($model->load($id)->getId()) {
            $feature_id = Mage::helper('folk_content')->getReturnFeatureId();
            if ($feature_id) {
                switch (Mage::helper('folk_content')->getReturnAction()) {
                    case 'back': // We don't actually care about users just coming back from Folk_Content with no action but we should probably check
                    case 'save':
                        // Loop through the CSV of features and add the returned one if missing
                        $found = false;
                        $featureArray = explode(',', $model->getFeatures());
                        for ($i = 0; $i < count($featureArray); $i++) {
                            if ($featureArray[$i] == $feature_id) {
                                $found = true;
                            }
                        }
                        if (!$found) {
                            array_push($featureArray, $feature_id);
                        }
                        $model->setFeatures(implode(',', $featureArray));
                        $model->save();
                        break;
                    case 'delete':
                        // Loop through the CSV of features and remove the deleted one
                        $featureArray = explode(',', $model->getFeatures());
                        for ($i = 0; $i < count($featureArray); $i++) {
                            if ($featureArray[$i] == $feature_id) {
                                unset($featureArray[$i]);
                            }
                        }
                        $model->setFeatures(implode(',', $featureArray));
                        $model->save();
                        break;
                }

            }
            // Be responsible and clear the returnUrl
            Mage::helper('folk_content')->clearReturnData();

            $this->_redirect('*/*/edit', array('id' => $id));
        } else {
            // It's all gone wrong so be responsible and clear the returnUrl before kicking the user out
            Mage::helper('folk_content')->clearReturnData();
            $this->_redirect('*/*');
        }
    }

    /**
     * delete issue - action
     *
     * @access public
     * @return void
     *
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $issue = Mage::getModel('folk_magazine/issue');
                $issue->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('folk_magazine')->__('Issue was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was an error deleting issue.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('folk_magazine')->__('Could not find issue to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete issue - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $issueIds = $this->getRequest()->getParam('issue');
        if (!is_array($issueIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('folk_magazine')->__('Please select issues to delete.')
            );
        } else {
            try {
                foreach ($issueIds as $issueId) {
                    $issue = Mage::getModel('folk_magazine/issue');
                    $issue->setId($issueId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('folk_magazine')->__('Total of %d issues were successfully deleted.', count($issueIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was an error deleting issues.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $issueIds = $this->getRequest()->getParam('issue');
        if (!is_array($issueIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('folk_magazine')->__('Please select issues.')
            );
        } else {
            try {
                foreach ($issueIds as $issueId) {
                $issue = Mage::getSingleton('folk_magazine/issue')->load($issueId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d issues were successfully updated.', count($issueIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was an error updating issues.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass issue category change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massIssuecategoryIdAction()
    {
        $issueIds = $this->getRequest()->getParam('issue');
        if (!is_array($issueIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('folk_magazine')->__('Please select issues.')
            );
        } else {
            try {
                foreach ($issueIds as $issueId) {
                $issue = Mage::getSingleton('folk_magazine/issue')->load($issueId)
                    ->setIssuecategoryId($this->getRequest()->getParam('flag_issuecategory_id'))
                    ->setIsMassupdate(true)
                    ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d issues were successfully updated.', count($issueIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was an error updating issues.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * get categories action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function categoriesAction()
    {
        $this->_initIssue();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * get child categories action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function categoriesJsonAction()
    {
        $this->_initIssue();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('folk_magazine/adminhtml_issue_edit_tab_categories')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'issue.csv';
        $content    = $this->getLayout()->createBlock('folk_magazine/adminhtml_issue_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'issue.xls';
        $content    = $this->getLayout()->createBlock('folk_magazine/adminhtml_issue_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'issue.xml';
        $content    = $this->getLayout()->createBlock('folk_magazine/adminhtml_issue_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('folk_content/folk_magazine/issue');
    }

    public function featuregridAction()
    {
        if (!Mage::registry('current_issue')) {
            Mage::register('current_issue', Mage::getModel('folk_magazine/issue')->load($this->getRequest()->getParam('id')));
        }

        $this->loadLayout(false);
        $this->renderLayout();
    }
}
