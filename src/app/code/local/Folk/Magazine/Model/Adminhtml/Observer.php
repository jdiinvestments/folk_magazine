<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Adminhtml observer
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Model_Adminhtml_Observer
{
    /**
     * check if tab can be added
     *
     * @access protected
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     * @author Ultimate Module Creator
     */
    protected function _canAddTab($product)
    {
        if ($product->getId()) {
            return true;
        }
        if (!$product->getAttributeSetId()) {
            return false;
        }
        $request = Mage::app()->getRequest();
        if ($request->getParam('type') == 'configurable') {
            if ($request->getParam('attributes')) {
                return true;
            }
        }
        return false;
    }

    /**
     * add the issue tab to categories
     *
     * @access public
     * @param Varien_Event_Observer $observer
     * @return Folk_Magazine_Model_Adminhtml_Observer
     * @author Ultimate Module Creator
     */
    public function addCategoryIssueBlock($observer)
    {
        $tabs = $observer->getEvent()->getTabs();
        $content = $tabs->getLayout()->createBlock(
            'folk_magazine/adminhtml_catalog_category_tab_issue',
            'category.issue.grid'
        )->toHtml();
        $serializer = $tabs->getLayout()->createBlock(
            'adminhtml/widget_grid_serializer',
            'category.issue.grid.serializer'
        );
        $serializer->initSerializerBlock(
            'category.issue.grid',
            'getSelectedIssues',
            'issues',
            'category_issues'
        );
        $serializer->addColumnInputName('position');
        $content .= $serializer->toHtml();
        $tabs->addTab(
            'issue',
            array(
                'label'   => Mage::helper('folk_magazine')->__('Issues'),
                'content' => $content,
            )
        );
        return $this;
    }

    /**
     * add the issuecategory tab to categories
     *
     * @access public
     * @param Varien_Event_Observer $observer
     * @return Folk_Magazine_Model_Adminhtml_Observer
     * @author Ultimate Module Creator
     */
    public function addCategoryIssuecategoryBlock($observer)
    {
        $tabs = $observer->getEvent()->getTabs();
        $content = $tabs->getLayout()->createBlock(
            'folk_magazine/adminhtml_catalog_category_tab_issuecategory',
            'category.issuecategory.grid'
        )->toHtml();
        $serializer = $tabs->getLayout()->createBlock(
            'adminhtml/widget_grid_serializer',
            'category.issuecategory.grid.serializer'
        );
        $serializer->initSerializerBlock(
            'category.issuecategory.grid',
            'getSelectedIssuecategories',
            'issuecategories',
            'category_issuecategories'
        );
        $serializer->addColumnInputName('position');
        $content .= $serializer->toHtml();
        $tabs->addTab(
            'issuecategory',
            array(
                'label'   => Mage::helper('folk_magazine')->__('Issue Categories'),
                'content' => $content,
            )
        );
        return $this;
    }

    /**
     * save issue - category relation
     *
     * @access public
     * @param Varien_Event_Observer $observer
     * @return Folk_Magazine_Model_Adminhtml_Observer
     * @author Ultimate Module Creator
     */
    public function saveCategoryIssueData($observer)
    {
        $post = Mage::app()->getRequest()->getPost('issues', -1);
        if ($post != '-1') {
            $post = Mage::helper('adminhtml/js')->decodeGridSerializedInput($post);
            $category = Mage::registry('category');
            $issueCategory = Mage::getResourceSingleton('folk_magazine/issue_category')
                ->saveCategoryRelation($category, $post);
        }
        return $this;
    }

    /**
     * save issue category - category relation
     *
     * @access public
     * @param Varien_Event_Observer $observer
     * @return Folk_Magazine_Model_Adminhtml_Observer
     * @author Ultimate Module Creator
     */
    public function saveCategoryIssuecategoryData($observer)
    {
        $post = Mage::app()->getRequest()->getPost('issuecategories', -1);
        if ($post != '-1') {
            $post = Mage::helper('adminhtml/js')->decodeGridSerializedInput($post);
            $category = Mage::registry('category');
            $issuecategoryCategory = Mage::getResourceSingleton('folk_magazine/issuecategory_category')
                ->saveCategoryRelation($category, $post);
        }
        return $this;
    }
}
