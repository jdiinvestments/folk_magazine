<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue model
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Model_Issue extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'folk_magazine_issue';
    const CACHE_TAG = 'folk_magazine_issue';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'folk_magazine_issue';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'issue';
    protected $_categoryInstance = null;

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('folk_magazine/issue');
    }

    /**
     * before save issue
     *
     * @access protected
     * @return Folk_Magazine_Model_Issue
     * @author Ultimate Module Creator
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * get the url to the issue details page
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getIssueUrl()
    {
        if ($this->getUrlKey()) {
            $urlKey = '';
            if ($prefix = Mage::getStoreConfig('folk_magazine/issue/url_prefix')) {
                $urlKey .= $prefix.'/';
            }
            $urlKey .= $this->getUrlKey();
            if ($suffix = Mage::getStoreConfig('folk_magazine/issue/url_suffix')) {
                $urlKey .= '.'.$suffix;
            }
            return Mage::getUrl('', array('_direct'=>$urlKey));
        }
        return Mage::getUrl('folk_magazine/issue/view', array('id'=>$this->getId()));
    }

    /**
     * check URL key
     *
     * @access public
     * @param string $urlKey
     * @param bool $active
     * @return mixed
     * @author Ultimate Module Creator
     */
    public function checkUrlKey($urlKey, $active = true)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $active);
    }

    /**
     * get the issue Html Content
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHtmlContent()
    {
        $html_content = $this->getData('html_content');
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();
        $html = $processor->filter($html_content);
        return $html;
    }

    /**
     * save issue relation
     *
     * @access public
     * @return Folk_Magazine_Model_Issue
     * @author Ultimate Module Creator
     */
    protected function _afterSave()
    {
        $this->getCategoryInstance()->saveIssueRelation($this);
        return parent::_afterSave();
    }

    /**
     * get category relation model
     *
     * @access public
     * @return Folk_Magazine_Model_Issue_Category
     * @author Ultimate Module Creator
     */
    public function getCategoryInstance()
    {
        if (!$this->_categoryInstance) {
            $this->_categoryInstance = Mage::getSingleton('folk_magazine/issue_category');
        }
        return $this->_categoryInstance;
    }

    /**
     * get selected categories array
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getSelectedCategories()
    {
        if (!$this->hasSelectedCategories()) {
            $categories = array();
            foreach ($this->getSelectedCategoriesCollection() as $category) {
                $categories[] = $category;
            }
            $this->setSelectedCategories($categories);
        }
        return $this->getData('selected_categories');
    }

    /**
     * Retrieve collection selected categories
     *
     * @access public
     * @return Folk_Magazine_Resource_Issue_Category_Collection
     * @author Ultimate Module Creator
     */
    public function getSelectedCategoriesCollection()
    {
        $collection = $this->getCategoryInstance()->getCategoryCollection($this);
        return $collection;
    }

    /**
     * Retrieve parent 
     *
     * @access public
     * @return null|Folk_Magazine_Model_Issuecategory
     * @author Ultimate Module Creator
     */
    public function getParentIssuecategory()
    {
        if (!$this->hasData('_parent_issuecategory')) {
            if (!$this->getIssuecategoryId()) {
                return null;
            } else {
                $issuecategory = Mage::getModel('folk_magazine/issuecategory')
                    ->load($this->getIssuecategoryId());
                if ($issuecategory->getId()) {
                    $this->setData('_parent_issuecategory', $issuecategory);
                } else {
                    $this->setData('_parent_issuecategory', null);
                }
            }
        }
        return $this->getData('_parent_issuecategory');
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * @author Ultimate Module Creator
     */
    public function getDefaultValues()
    {
        $values = array();
        $values['status'] = 1;
        return $values;
    }

    public function getSelectedFeaturesCollection()
    {
        if (!$this->hasData('_feature_collection')) {
            if (!$this->getId()) {
                return new Varien_Data_Collection();
            } else {
                $collection = Mage::getResourceModel('folk_magazine/feature_collection')
                    ->addFieldToFilter('category_id', $this->getId());
                $this->setData('_feature_collection', $collection);
            }
        }
        return $this->getData('_feature_collection');
    }
    
}
