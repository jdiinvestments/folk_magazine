<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue list block
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author Ultimate Module Creator
 */
class Folk_Magazine_Block_Issue_List extends Mage_Core_Block_Template
{
    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $issues = Mage::getResourceModel('folk_magazine/issue_collection')
                         ->addStoreFilter(Mage::app()->getStore())
                         ->addFieldToFilter('status', 1);
        $issues->setOrder('title', 'asc');
        $this->setIssues($issues);
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return Folk_Magazine_Block_Issue_List
     * @author Ultimate Module Creator
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock(
            'page/html_pager',
            'folk_magazine.issue.html.pager'
        )
        ->setCollection($this->getIssues());
        $this->setChild('pager', $pager);
        $this->getIssues()->load();
        return $this;
    }

    /**
     * get the pager html
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
