<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue Category admin controller
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Adminhtml_Magazine_IssuecategoryController extends Folk_Magazine_Controller_Adminhtml_Magazine
{
    /**
     * init the issue category
     *
     * @access protected
     * @return Folk_Magazine_Model_Issuecategory
     */
    protected function _initIssuecategory()
    {
        $issuecategoryId  = (int) $this->getRequest()->getParam('id');
        $issuecategory    = Mage::getModel('folk_magazine/issuecategory');
        if ($issuecategoryId) {
            $issuecategory->load($issuecategoryId);
        }
        Mage::register('current_issuecategory', $issuecategory);
        return $issuecategory;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('folk_magazine')->__('Folk Magazine'))
             ->_title(Mage::helper('folk_magazine')->__('Issue Categories'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit issue category - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $issuecategoryId    = $this->getRequest()->getParam('id');
        $issuecategory      = $this->_initIssuecategory();
        if ($issuecategoryId && !$issuecategory->getId()) {
            $this->_getSession()->addError(
                Mage::helper('folk_magazine')->__('This issue category no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getIssuecategoryData(true);
        if (!empty($data)) {
            $issuecategory->setData($data);
        }
        Mage::register('issuecategory_data', $issuecategory);
        $this->loadLayout();
        $this->_title(Mage::helper('folk_magazine')->__('Folk Magazine'))
             ->_title(Mage::helper('folk_magazine')->__('Issue Categories'));
        if ($issuecategory->getId()) {
            $this->_title($issuecategory->getTitle());
        } else {
            $this->_title(Mage::helper('folk_magazine')->__('Add issue category'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new issue category action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save issue category - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('issuecategory')) {
            try {
                $data = $this->_filterDates($data, array('make_active_from' ,'make_active_to'));
                $issuecategory = $this->_initIssuecategory();
                $issuecategory->addData($data);
                $imageName = $this->_uploadAndGetName(
                    'image',
                    Mage::helper('folk_magazine/issuecategory_image')->getImageBaseDir(),
                    $data
                );
                $issuecategory->setData('image', $imageName);
                $categories = $this->getRequest()->getPost('category_ids', -1);
                if ($categories != -1) {
                    $categories = explode(',', $categories);
                    $categories = array_unique($categories);
                    $issuecategory->setCategoriesData($categories);
                }
                $issuecategory->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('folk_magazine')->__('Issue Category was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $issuecategory->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setIssuecategoryData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                if (isset($data['image']['value'])) {
                    $data['image'] = $data['image']['value'];
                }
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was a problem saving the issue category.')
                );
                Mage::getSingleton('adminhtml/session')->setIssuecategoryData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('folk_magazine')->__('Unable to find issue category to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete issue category - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $issuecategory = Mage::getModel('folk_magazine/issuecategory');
                $issuecategory->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('folk_magazine')->__('Issue Category was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was an error deleting issue category.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('folk_magazine')->__('Could not find issue category to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete issue category - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {
        $issuecategoryIds = $this->getRequest()->getParam('issuecategory');
        if (!is_array($issuecategoryIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('folk_magazine')->__('Please select issue categories to delete.')
            );
        } else {
            try {
                foreach ($issuecategoryIds as $issuecategoryId) {
                    $issuecategory = Mage::getModel('folk_magazine/issuecategory');
                    $issuecategory->setId($issuecategoryId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('folk_magazine')->__('Total of %d issue categories were successfully deleted.', count($issuecategoryIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was an error deleting issue categories.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $issuecategoryIds = $this->getRequest()->getParam('issuecategory');
        if (!is_array($issuecategoryIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('folk_magazine')->__('Please select issue categories.')
            );
        } else {
            try {
                foreach ($issuecategoryIds as $issuecategoryId) {
                $issuecategory = Mage::getSingleton('folk_magazine/issuecategory')->load($issuecategoryId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d issue categories were successfully updated.', count($issuecategoryIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('folk_magazine')->__('There was an error updating issue categories.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * get categories action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function categoriesAction()
    {
        $this->_initIssuecategory();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * get child categories action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function categoriesJsonAction()
    {
        $this->_initIssuecategory();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('folk_magazine/adminhtml_issuecategory_edit_tab_categories')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'issuecategory.csv';
        $content    = $this->getLayout()->createBlock('folk_magazine/adminhtml_issuecategory_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'issuecategory.xls';
        $content    = $this->getLayout()->createBlock('folk_magazine/adminhtml_issuecategory_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'issuecategory.xml';
        $content    = $this->getLayout()->createBlock('folk_magazine/adminhtml_issuecategory_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('folk_content/folk_magazine/issuecategory');
    }
}
