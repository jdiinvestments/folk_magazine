<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue view block
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Issue_View extends Mage_Core_Block_Template
{
    /**
     * get the current issue
     *
     * @access public
     * @return mixed (Folk_Magazine_Model_Issue|null)
     * @author Ultimate Module Creator
     */
    public function getCurrentIssue()
    {
        return Mage::registry('current_issue');
    }
}
