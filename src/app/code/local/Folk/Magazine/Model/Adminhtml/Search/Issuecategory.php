<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Model_Adminhtml_Search_Issuecategory extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Folk_Magazine_Model_Adminhtml_Search_Issuecategory
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('folk_magazine/issuecategory_collection')
            ->addFieldToFilter('title', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $issuecategory) {
            $arr[] = array(
                'id'          => 'issuecategory/1/'.$issuecategory->getId(),
                'type'        => Mage::helper('folk_magazine')->__('Issue Category'),
                'name'        => $issuecategory->getTitle(),
                'description' => $issuecategory->getTitle(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/magazine_issuecategory/edit',
                    array('id'=>$issuecategory->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
