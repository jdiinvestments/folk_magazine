<?php
/**
 * Folk_Magazine extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Folk
 * @package        Folk_Magazine
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Issue list on category page block
 *
 * @category    Folk
 * @package     Folk_Magazine
 * @author      Ultimate Module Creator
 */
class Folk_Magazine_Block_Catalog_Category_List_Issue extends Mage_Core_Block_Template
{
    /**
     * get the list of issues
     *
     * @access protected
     * @return Folk_Magazine_Model_Resource_Issue_Collection
     * @author Ultimate Module Creator
     */
    public function getIssueCollection()
    {
        if (!$this->hasData('issue_collection')) {
            $category = Mage::registry('current_category');
            $collection = Mage::getResourceSingleton('folk_magazine/issue_collection')
                ->addStoreFilter(Mage::app()->getStore())
                ->addFieldToFilter('status', 1)
                ->addCategoryFilter($category);
            $collection->getSelect()->order('related_category.position', 'ASC');
            $this->setData('issue_collection', $collection);
        }
        return $this->getData('issue_collection');
    }
}
